from flask_restful import Resource
from flask import Blueprint, request
from uuid import uuid4
from song_downloader import SongDownloader


def downloader_func(api, db, SongsDB, PlaylistsDB):
    downloader = Blueprint("downloader", __name__)
    sd = SongDownloader()

    class DownloadSong(Resource):
        def get(self):
            return {
                "songs_downloaded": sd.update_progress[0],
                "songs_downloading": sd.update_progress[1],
            }

        def post(self):
            if "song_url" in request.form:
                dict = sd.download_song(request.form["song_url"])
                id = str(dict["song_id"])
                entry = SongsDB(
                    song_id=id,
                    song_name=dict["song_name"],
                    song_link=f"songs/{id}.webm",
                )
                db.session.add(entry)
                db.session.commit()
                return "downloaded", 200

    class DownloadPlaylist(Resource):
        def get(self):
            return {
                "songs_downloaded": sd.update_progress[0],
                "songs_downloading": sd.update_progress[1],
            }

        def post(self):
            if "song_url" and "playlist_name" in request.form:
                dict = sd.download_playlist(request.form["song_url"])
                playlist_name = request.form["playlist_name"]
                playlist_songs = []
                for song in dict:
                    id = str(song["song_id"])
                    entry = SongsDB(
                        song_id=id,
                        song_name=song["song_name"],
                        song_link=f"songs/{id}.webm",
                    )
                    playlist_songs.append(id)
                    db.session.add(entry)
                db.session.add(
                    PlaylistsDB(
                        playlist_id=str(uuid4()),
                        playlist_name=playlist_name,
                        playlist_songs=f"{playlist_songs}",
                    )
                )
                db.session.commit()

                return "downloaded", 200

    api.add_resource(DownloadSong, "/api/songs/downloadsong/")
    api.add_resource(DownloadPlaylist, "/api/songs/downloadplaylist/")
    return downloader
