from flask_restful import Resource, fields, marshal_with
from flask import Blueprint, send_file, request, current_app
from werkzeug.utils import secure_filename
from uuid import uuid4
from zipfile import ZipFile
import io
import os

ALLOWED_EXTENSIONS = ["mp3", "flac", "wav", "mp4", "m4a", "webm"]
# TODO separate class responcibilities
"""
A global playlist, serves for editing songs in a global scope, deleting 
songs/changing their name here, will result in database & song file changes.

Allsongs playlist module handles: adding new songs, editing songs, deleting
songs permanently, serving all songs for playback and searching through all songs.

Note: Other playlists are handeled by the playlist module, song editing in
playlist module scope is not possible, you can only delete them from the playlist.
"""


def allsong_playlist_func(api, songdb_com, SongsDB):
    allsong_playlist = Blueprint("allsong_playlist", __name__)
    """
    Songs class provides access to all of the available songs in one place
    flask configuration "SONGS_PER_PAGE" can be change to limit the output of this class.
    Methods: GET
    Endpoints: 
        GET
            /api/playlist/allsongs/<page>
            /api/playlist/allsongs/
    """

    class Songs(Resource):
        @marshal_with(songdb_com.get_resource_fields(["song_author"]))
        def get(self, page=0):
            allsongs = SongsDB.query.all()
            if page == 0:
                return allsongs[: current_app.config["SONGS_PER_PAGE"]]
            else:
                return allsongs[
                    current_app.config["SONGS_PER_PAGE"] : (page + 1)
                    * current_app.config["SONGS_PER_PAGE"]
                ]

    """
    Song class provides access to a single song file as a streamable file (for song downloading see the "SongDownloader" class)
    Also takes care of editing and creating new song files. User can upload music directly through this class.
    Methods: GET, POST, DELETE
    Endpoints:
        GET, DELETE
            /api/playlist/allsongs/song/<song_id> - returns a song for direct streaming or deletes it when DELETE is used
        POST
            /api/playlist/allsongs/song
                - Accepts any dictionary object with correct column names.(database columns)
                - "song_id" - if provided, the object with that id will be edited instead of created. Do not provide otherwise.
    """

    class Song(Resource):
        def get(self, song_id):
            return send_file(SongsDB.query.filter_by(song_id=song_id).first().song_link)

        def post(self):
            data = request.form.to_dict()
            # TODO calculate hash sums of new songs incase of duplicates
            try:
                songdb_com.edit_data(data, data["song_id"])
                return 200
            except KeyError:
                if "song_file" in request.files:
                    file = request.files["song_file"]
                else:
                    return "Song file is required", 404
                extension = file.filename.split(".")[-1]
                if extension in ALLOWED_EXTENSIONS:
                    id = str(uuid4())
                    data["song_id"] = id
                    data["song_link"] = f"songs/{id}.{extension}"
                    file.save(f"songs/{id}.{extension}")
                    songdb_com.insert_data(data)
                    return str(id), 200
                else:
                    return (
                        f"Unsupported filetype, supported filetypes are:{ALLOWED_EXTENSIONS}",
                        415,
                    )

        def delete(self, song_id):
            song_id_arr = song_id.split("&")
            for each in song_id_arr:
                item = SongsDB.query.filter_by(song_id=each).all()
                if item:
                    for each in item:
                        os.remove(each.song_link)
                        songdb_com.delete_data(each)
                    return 200
                else:
                    return "Song doesn't exist", 404

    """
    SongDownload class provides a way to download songs directly and handles zipping and sending multiple song downloads.
    Multiple songs can be separates using &, and when this symbol is used, the songs get automatically zipped and sent out.
    Methods: GET
    Endpoints:
        GET
            /api/playlist/allsongs/song/download/<song_ids>
    """

    class SongDownload(Resource):
        def get(self, song_ids):
            song_id_arr = song_ids.split("&")
            if len(list(song_id_arr)) > 1:
                memory_zip = io.BytesIO()
                song_zip = ZipFile(memory_zip, "w")
                for song_id in song_id_arr:
                    song = SongsDB.query.filter_by(song_id=song_id).first()
                    song_name = (
                        secure_filename(song.song_name)
                        + "."
                        + song.song_link.split(".")[1]
                    )
                    song_zip.write(song.song_link, f"{song_name}")
                song_zip.close()
                memory_zip.seek(0)
                return send_file(
                    memory_zip, as_attachment=True, download_name="songs.zip"
                )
            else:
                song = SongsDB.query.filter_by(song_id=song_ids).first()
                return send_file(
                    song.song_link,
                    as_attachment=True,
                    download_name=secure_filename(song.song_name)
                    + "."
                    + song.song_link.split(".")[1],
                )
    """
    SearchSongs class provides a way to search through specific columns.
    Methods: GET
    Endpoints:
        GET
            /api/playlist/allsongs/search/name/<query>
    """
    class SearchSongs(Resource):
        @marshal_with(songdb_com.get_resource_fields())
        def get(self, query):
            return songdb_com.search(["song_name", "song_author"], query, "song_id")

    class PageStats(Resource):
        def get(self):
            return {
                "pages": int(
                    (SongsDB.query.count() / current_app.config["SONGS_PER_PAGE"])
                    + 0.99
                ),
                "songs": SongsDB.query.count(),
            }

    api.add_resource(
        Songs, "/api/playlist/allsongs/", "/api/playlist/allsongs/<int:page>"
    )
    api.add_resource(
        Song,
        "/api/playlist/allsongs/song/",
        "/api/playlist/allsongs/song/<string:song_id>",
    )
    api.add_resource(
        SongDownload, "/api/playlist/allsongs/song/download/<string:song_ids>"
    )
    api.add_resource(SearchSongs, "/api/playlist/allsongs/search/name/<string:query>")
    api.add_resource(PageStats, "/api/playlist/allsongs/pagestats")
    return allsong_playlist
