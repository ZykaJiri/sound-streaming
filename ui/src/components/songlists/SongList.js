import Song from "./Song"

const SongList = ({currentlyPlayingID,songObjects,selectedSongObjects,setSelectedSongObjects,onClickSong,onSearch}) => {
    console.log(selectedSongObjects)
    const onChecked = (songObject) =>{
        const selectedSongs = new Set(selectedSongObjects)
        if(!selectedSongObjects.has(songObject)){
            selectedSongs.add(songObject)
        } else {
            selectedSongs.delete(songObject)
        }
        setSelectedSongObjects(selectedSongs)
    }

    return (
        <div style={{margin:"auto",minWidth:"400px",maxHeight:"500px",overflow:"scroll",overflowY:"auto",overflowX:"hidden"}} className="d-inline-block p-2 border">
            <div className="input-group mb-3">
                <input type="text" onChange={(e)=>onSearch(e.target.value)} className="form-control" placeholder="Seach all songs(min. 3 chars)" aria-label="" aria-describedby="basic-addon1"/>
            </div>
            {songObjects.length === 0 && <p className="h5">No songs found</p>}
            {songObjects.length>0 &&songObjects.map((songObject,index) =><Song key={songObject.song_id} songObject={songObject} onClickSong={onClickSong} isChecked={[...selectedSongObjects].filter((selObject)=>selObject.song_id === songObject.song_id).length > 0} onChecked={onChecked} songIndex={index} isSelected={songObject.song_id === currentlyPlayingID}/>)}
        </div>
    )
}

export default SongList
