import { Download } from 'react-bootstrap-icons';
const Song = ({songObject, onClickSong, songIndex, isSelected, onChecked, isChecked}) => {
    console.log(isChecked)
    return (
        <div className="text-light bg-dark">
            <div
            key={songObject.song_id} 
            id={songObject.song_id}
            onClick={()=>onClickSong(songObject.song_id)}
            style={{overflow:"hidden", maxHeight:"3em"}} 
            className={`${isSelected?"bg-primary":"bg-dark"} song m-0 p-2 hover-shadow text-wrap text-light`}>
                <div className="d-flex flex-row gap-2">
                    <span>{songIndex+1}</span>
                    <span>{songObject.song_name}</span>
                    <a className="ms-auto text-decoration-none" href={`/api/playlist/allsongs/song/download/${songObject.song_id}`}><Download onClick={(e)=>{e.stopPropagation();}} title="Download"/></a>
                    <input onClick={(e)=>{e.stopPropagation()}} onChange={()=>onChecked(songObject)} type="checkbox" checked={isChecked}/>
                </div>
            </div>
            <hr style={{width:"100%",margin:"auto"}}/>
        </div>
    )
}

export default Song
