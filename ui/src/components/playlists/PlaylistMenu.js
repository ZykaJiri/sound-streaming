import Playlist from "./Playlist"

const PlaylistMenu = ({playlistObjects,usingPlaylistID, setUsingPlaylistID, setPage}) => {
    return (
        <div>
            <div style={{margin:"auto",minWidth:"25%",maxHeight:"500px",overflowY:"auto",overflowX:"hidden"}} className="d-inline-block border mr-1 p-2">
                <Playlist setUsingPlaylistID={setUsingPlaylistID} playlistIndex={""} isSelected={usingPlaylistID==="allsongs"} playlistObject={{"playlist_name":"All songs","playlist_id":"allsongs"}}/>
                {playlistObjects.map((playlistObject,index) =><Playlist key={playlistObject.playlist_id} isSelected={usingPlaylistID===playlistObject.playlist_id} setUsingPlaylistID={setUsingPlaylistID} setPage={setPage} playlistIndex={index} playlistObject={playlistObject}/>)}
            </div>
        </div>
    )
}

export default PlaylistMenu
