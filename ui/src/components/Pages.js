import { Pagination } from "react-bootstrap"
const Pages = ({page,setPage,pagesNum}) => {
    return (
        <div style={{margin:"auto"}}>
            <Pagination>
                <Pagination.First onClick={()=>{setPage(0);}} />
                <Pagination.Prev onClick={()=>{page-1 >= 0 && setPage(page-1);}}/>
                {Array.from(Array(pagesNum), (e, i) =>
                    <Pagination.Item key={i} active={page === i} onClick={()=>{setPage(i);}}>{i+1}</Pagination.Item>
                )}
                <Pagination.Next onClick={()=>{page+1 < pagesNum && setPage(page+1);}}/>
                <Pagination.Last onClick={()=>{setPage(pagesNum-1);}}/>
            </Pagination>
        </div>
    )
}

export default Pages
